@extends('user.layouts.pattern')
<!DOCTYPE html>
<html lang="en">
<head>
	<base href="{{asset('')}}">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="image/icon_tab.png">
	<title>Welcome to BCS Shop</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/webstyle2.css">
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</head>
<body>
	@section('NoiDung')
	<!-- Slide -->
	<section>
		<div class="container fixDisplay">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div id="carousel-id" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carousel-id" data-slide-to="0" class=""></li>
							<li data-target="#carousel-id" data-slide-to="1" class=""></li>
							<li data-target="#carousel-id" data-slide-to="2" class="active"></li>
						</ol>
						<div class="carousel-inner">
							@for($i = 0; $i < ($slide->count() - 1); $i++)
							<div class="item" >
								<img alt="First slide" src="image/{!!$slide[$i]->image!!}" width="100%">
							</div>
							@endfor
							<div class="item active">
								<img alt="Third slide" src="image/banner3.png" width="100%">
							</div>
						</div>
						<a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
						<a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Sản phẩm -->
	<section>
		<div class="container list-product">
			<div class="col-xs-12 col-md-3">
				<div>
					<h2><br></h2>
				</div>
				<div class="btn-group-vertical">
					@foreach($danhmucsanpham as $loai)
					<button type="button" onclick="link_danhmucsanpham({{$loai->id}})" class="btn btn-default">{!!$loai->hang!!}</button>
					@endforeach

					<!-- <div class="btn-group">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							DROPDOWN
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="#">DROPDOWN LINK</a></li>
							<li><a href="#">DROPDOWN LINK</a></li>
						</ul>
					</div> -->
				</div>
			</div>
			<div class="col-xs-12 col-md-9">
				<h2>What's new?</h2>
				<table class="table table-bordered">
					<tbody>
						@for($i = 0; $i < $sanpham->count(); $i = $i + 3)
						<tr>
							@for($j = $i; $j < $i + 3; $j++)
							@if($j < $sanpham->count())
							<td>
								<a href="details.html"><img alt="NMD" src="image/{!!$sanpham[$j]->anh1!!}"></a>
								<p></p>
								<b> {!!$sanpham[$j]->ten!!} <a href=""><i class="fas fa-shopping-cart"></i></a></b>
								<p> Giá: {!!$sanpham[$j]->dongia!!} VNĐ</p>
							</td>
							@else 
							<td>
								<p></p>
								<p></p>
								<p></p>
								<p></p>
								<p></p>
								<p></p>
								<p></p>
							</td>
							@endif
							@endfor		          		  
						</tr>
						@endfor
					</tbody>
				</table>
			</div>
		</div>
		<div class="row control">
			<ul class="pagination">
				{{$sanpham->links()}}
				<!-- <li><a href="#">&laquo;</a></li>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#">&raquo;</a></li> -->
			</ul>

		</div>	
	</section>
	@endsection
	<!-- dẫn link phân loại -->
	<script language="javascript">
		function link_danhmucsanpham(id_danhmuc){
			var link = new String("danhmucsanpham/" + id_danhmuc)
			location.href = link;
		}
	</script>
</body>
</html>
	
<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="image/icon_tab.png">
  <title>Welcome to BCS Shop</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/webstyle2.css">
  <link rel="stylesheet" href="css/form.css">
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</head>

<body>
  <header class="container-fluid">
    <div class="header-top">
      <!-- <p>Chào mừng đến BCS Shop</p> -->
      <div class="left">
        <a href=""><i class="fas fa-shopping-cart"></i></a>
      </div>
      <div class="left">
        <input type="button" value="Đăng nhập">
      </div>
      <div class="left">
        <input type="button" value="Đăng kí">
      </div>
    </div>

    <div class="row header-bot">
      <div class="col-md-3">
        <a href=""><i class="fas fa-shoe-prints"></i></a>
      </div>
      <div class="col-xs-12 col-md-5">
        <input type="text" placeholder="Tìm kiếm trong BCS Shop">
      </div>
      <div class="col-xs-12 col-md-4">
        <button type="submit"><i class="fas fa-search"></i></button>
      </div>
    </div>



  </header>

  <section>
    <h2 id="Chao">Thông tin giao hàng</h2>
    <div class="container login">
      <form action="/action_page.php">
        <div class="row">
          <div class="col-25">
            <label for="Địa chỉ">Tên người nhận*</label>
          </div>
          <div class="col-75">
            <input type="text" id="" name="fullName" placeholder="Tên người nhận" value="Lê Đình Mạnh" required="Chưa nhập họ">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="Tên">Số điện thoại*</label>
          </div>
          <div class="col-75">
            <input type="text" id="" name="SDT" placeholder="Số điện thoại người nhận" value="0333115118" required="Chưa nhập tên">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="User-name">Địa chỉ*</label>
          </div>
          <div class="col-75">
            <input type="text" id="" name="address" placeholder="Địa chỉ người nhận" value="Trên giời" required="Chưa nhập địa chỉ">
          </div>
        </div>
        <div class="row">
          <div class="col-25"><h3>Tổng số tiền:</h3></div>
          <div class="col-75"><h3>630</h3></div>
        </div>
        <div class="row">
          <input type="submit" value="Xong">
        </div>   
      </form>

    </div>
    <!-- <div class="goods">
      <div><h2 id ="sanPham">Tổng số tiền: <span> 630</span></h2></div>
    </div>
    <div id="finish"><button>Xong</button></div> -->
  </section>
  <section>
    <div class="container-fluid footer">
      <div class="col-xs-12 col-md-4 ">
        <h4>Shop Mạnh giày</h4>
        <p>Chuyên cung cấp các loại giày
          từ real đến fake với giá cả phải
        chăng.</p>
      </div>
      <div id="middle" class="col-md-4 col-xs-12">
        <h4>Địa chỉ liên hệ</h4>
        <p>Số 6, ngõ 9, đường 69, Hoàng Mai, Hà Nội</p>
        <p>Số điện thoại: 0696969690</p>
        <p>Email: thich69@gmail.com</p>
      </div>
      <div class="col-md-4 col-xs-12">
        <h4>Địa chỉ liên hệ</h4>
        <p>Số 6, ngõ 9, đường 69, Hoàng Mai, Hà Nội</p>
        <p>Số điện thoại: 0696969690</p>
        <p>Email: thich69@gmail.com</p>
      </div>
    </div>
  </section>

  <script>
    function validate() 
    {
      // body...
      var password = document.getElementById("password").value;
      var re_password = document.getElementById("re-password").value;
      if (password!= re_password){
        alert("Mật khẩu không khớp");
        return false;
      }
      return true;
    }

  </script>
</body>
</html>

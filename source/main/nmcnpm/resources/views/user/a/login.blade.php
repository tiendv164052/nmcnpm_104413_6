@extends('user.layouts.pattern')
<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="{{asset('image\icon_tab.png')}}">
  <title>Welcome to BCS Shop</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/webstyle2.css')}}">
  <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>

  <style>
  * {
    box-sizing: border-box;
  }

  #Chao{
    padding-left:25%;
  }

  .login {
    padding-top: 30px;
    padding-bottom: 50px;
  }

  input[type=text], input[type=password], select, textarea {
    width: 100%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    resize: vertical;
  }

  label {
    padding: 12px 12px 12px 0;
    display: inline-block;
  }

  input[type=submit] {
    background-color: #ff6344;
    color: white;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    float: right;
  }

  input[type=submit]:hover {
    background-color: #45a049;
  }

  .container {
    width:50%;
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
  }

  .col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
  }

  .col-75 {
    float: left;
    width: 75%;
    margin-top: 6px;
  }

  /* Clear floats after the columns */
  .row:after {
    content: "";
    display: table;
    clear: both;
  }

  /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
  @media screen and (max-width: 600px) {
    .col-25, .col-75, input[type=submit] {
      width: 100%;
      margin-top: 0;
    }
  }
</style>
</head>
<body>
  @section('NoiDung')
  <section class="login">
    <h2 id="Chao">Chào mừng đến BCS Shop. Đăng nhập tại đây!</h2>
    <div class="container login">
      <form action="/action_page.php">
        <div class="row">
          <div class="col-25">
            <label for="fname">Tên đăng nhập*</label>
          </div>
          <div class="col-75">
            <input type="text" id="fname" name="firstname" placeholder="Tên đăng nhập" required="Chưa nhập tên đăng nhập">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="lname">Mật khẩu*</label>
          </div>
          <div class="col-75">
            <input type="password" id="lname" name="lastname" placeholder="Mật khẩu" required="Chưa nhập mật khẩu">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="country"></label>
          </div>
          <div class="col-75">
            <a href="">Quên mật khẩu</a>
          </div>
        </div>
        <div class="row">
          <input type="submit" value="Đăng nhập">
        </div>
      </form>
    </div>
  </section>
  @endsection
</body>
</html>

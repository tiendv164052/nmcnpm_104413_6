@extends('user.layouts.pattern')
<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="{{asset('image/icon_tab.png')}}">
  <title>Welcome to BCS Shop</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/webstyle2.css')}}">
  <script src="{{asset('jquery/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>

</head>
<body>
  @section('NoiDung')
  <section>
    <div class="container details">
          <div class="col-md-1">
            <img src="{{asset('image/anh_1.png')}}" alt="ảnh 1">
            <br><br>
            <img src="{{asset('image/anh_2.png')}}" alt="ảnh 2">
            <br><br>
            <img src="{{asset('image/anh_3.png')}}" alt="ảnh 3">
          </div>
          <div class="col-md-7">
            <img src="{{asset('image/Big_1.png')}}" alt="">
          </div>
          <div class="col-xs-12 col-md-4">
          <div class="rt">
            <div class="bp">
              <h3>AIR STRONG TRAINING</h3>
              <small>Men shoe, Nike Jordan</small>
            </div>
            <div class="cost">
              <div class="pl"><p>1.090.000 VNĐ</p></div>
            </div>
            <div class="review">
              <h4>REVIEW SẢN PHẨM</h4>
            </div>
            <hr>
            <div class="text1">
              <p>Chúng tôi có một trong những bộ sưu tập lớn nhất sản phẩm luyện thể thao như bống rổ, bóng đá, bóng vầu dục và golf. Với bộ sưu tập  này bạn có thể trang bị từ đầu tới chân cho mình và các vận động viên của mình...</p>
            </div>
            <div class="review1">
              <h4>CHỌN MÀU SẮC</h4>
            </div>
            <hr>
            <div class="color-choser">
              <ul>
                <li><a class="black-bg " href="#">black</a></li>
                <li><a class="red-bg" href="#">red</a></li>
                <li><a class="pink-bg" href="#">pink</a></li>
                <li><a class="gray-bg" href="#">gray</a></li>
                <li><a class="lightgreen-bg" href="#">green</a></li>
              </ul>
              
            </div>
            <div class="review1">
              <h4>CHỌN SIZE / SỐ LƯỢNG</h4>
            </div>
            <hr>
            <div class="form">
              <div class="formleft">
                <form >
                  <input class ="age" list="Size" name="Size" placeholder="Kích cỡ">
                    <datalist id="Size">
                      <option value="38">
                      <option value="39">
                      <option value="40">
                      <option value="41">
                      <option value="42">
                    </datalist>
                  <input class="age1" list="So_luong" name="Số lượng" placeholder="Số Lượng">
                    <datalist id="So_luong">
                      <option value="1">
                      <option value="2">
                      <option value="3">
                      <option value="4">
                      <option value="5">
                    </datalist>
                </form>
              </div>              
            </div>
            <div class="button">
              <button type="button" class="btn btn-warning">CHO VÀO GIỎ</button>
              <button type="button" class="btn btn-success">MUA NGAY</button>
            </div>
          </div>
        </div>
        </div>

        <div class="container inform">
          <div class="tab">
              <button class="tablinks" onclick="openCity(event, 'tab1')" id="defaultOpen">CHI TIẾT SẢN PHẨM</button>
              <button class="tablinks" onclick="openCity(event, 'tab2')">THÔNG SỐ KĨ THUẬT</button>
              <button class="tablinks" onclick="openCity(event, 'tab3')">HƯỚNG DẪN SỬ DỤNG</button>
            </div>

            <div id="tab1" class="tabcontent">
              <p>
                Chúng tôi có một trong những bộ sưu tập lớn nhất sản phẩm luyện tập thể thao như bóng rổ, bóng đá, bóng bầu dục, và golf. Với bộ sưu tập này, bạn có thể trang bị từ đầu tới chân cho mình và các vận 
                động viên của mình. Chúng tôi còn có giày đinh Nike!
                GIAYNIKE - Giày Nike Chạy Bộ Nam Nike Air Zoom Pegasus 32 Nam - 749340-402
                Ngoài ra, với phong cách nổi bật cho nam giới, phụ nữ, và trẻ em, chúng tôi cung cấp các sản phẩm phổ biến nhất của Nike cho cả gia đình. Chẳng hạn như Nike Free. Sản phẩm được thiết kế để tăng cường 
                chuyển động tự nhiên của bàn chân, giúp bạn tự do hơn với mỗi bước đi. Khám phá phong cách giày chạy bộ, luyện tập và sử dụng hàng ngày!
                Bạn đang tìm kiếm một đôi giày chạy bộ hoàn hảo? The Pegasus bảo vệ đôi bàn chân của bạn với ba đặc điểm riêng biệt: đệm đàn hồi (nhờ có không gian Nike Zoom ở gót giày), dễ đi (với phần đế ngoài 
                thẳng giúp cải thiện khả năng nhấc bước cao của bạn),
              </p>
            </div>

            <div id="tab2" class="tabcontent">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus quaerat doloremque quidem ratione adipisci perspiciatis animi facilis soluta laboriosam porro? Explicabo nobis reprehenderit delectus, dolorem ex, cumque consequuntur! Asperiores, odio!</p> 
            </div>

            <div id="tab3" class="tabcontent">
              <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
              </p>
            </div>
        </div>
  </section>
  @endsection 

  <script language="javascript">

    function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";
    }

// Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
 </script>
</body>
</html>

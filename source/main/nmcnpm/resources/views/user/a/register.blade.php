@extends('user.layouts.pattern')
<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="{{asset('image/icon_tab.png')}}">
  <title>Welcome to BCS Shop</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/webstyle2.css')}}">
  <link rel="stylesheet" href="{{asset('css/form.css')}}">
  <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>

</head>
<body>
  @section('NoiDung')

  <section class="login">
    <h2 id="Chao">Chào mừng đến BCS Shop. Đăng kí tại đây!</h2>
    <div class="container login">
      <form action="/action_page.php">
        <div class="row">
          <div class="col-25">
            <label for="Họ">Họ*</label>
          </div>
          <div class="col-75">
            <input type="text" id="" name="Họ" placeholder="Họ" required="Chưa nhập họ">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="Tên">Tên*</label>
          </div>
          <div class="col-75">
            <input type="text" id="" name="Tên" placeholder="Tên" required="Chưa nhập tên">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="User-name">Tên đăng nhập*</label>
          </div>
          <div class="col-75">
            <input type="text" id="" name="User-name" placeholder="Tên đăng nhập" required="Chưa nhập tên đăng nhập">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="password">Mật khẩu*</label>
          </div>
          <div class="col-75">
            <input type="password" id="password" name="password" placeholder="Mật khẩu" required="Chưa nhập mật khẩu">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="re-password">Nhập lại mật khẩu*</label>
          </div>
          <div class="col-75">
            <input type="password" id="re-password" name="re-password" placeholder="Nhập lại mật khẩu" required="Chưa nhập lại mật khẩu">
          </div>
        </div>
        
        <div class="row">
          <div class="col-25">
            <label for="email">Email*</label>
          </div>
          <div class="col-75">
            <input type="email" id="" name="email" placeholder="Email" required="Chưa nhập Email">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="địa chỉ">Địa chỉ*</label>
          </div>
          <div class="col-75">
            <input type="text" id="" name="Địa chỉ" placeholder="Địa chỉ" required="">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="Số điện thoại">Số điện thoại*</label>
          </div>
          <div class="col-75">
            <input type="text" id="" name="Số điện thoại" placeholder="Số điện thoại" required="">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="ngày sinh">Ngày sinh*</label>
          </div>
          <div class="col-75">
            <input type="date" id="" name="Ngày sinh" placeholder="Ngày sinh" required="">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="giới tính">Giới tính*</label>
          </div>
          <div class="col-75">
            <select name="giới tính">
              <option value="nam">Nam</option>
              <option value="nữ">Nữ</option>
            </select>
          </div>
        </div>
        <div class="row">
          <input type="submit" value="Đăng kí" onclick="return validate()">
        </div>
      </form>
    </div>
  </section>
  @endsection 

  <script>
    function validate() 
    {
      // body...
      var password = document.getElementById("password").value;
      var re_password = document.getElementById("re-password").value;
      if (password!= re_password){
        alert("Mật khẩu không khớp");
        return false;
      }
      return true;
    }

  </script>
</body>
</html>

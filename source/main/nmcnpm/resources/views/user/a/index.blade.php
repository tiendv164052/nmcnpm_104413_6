@extends('user.layouts.pattern')
<!DOCTYPE html>
<html lang="en">
<head>
	<base href="{{asset('')}}">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="image/icon_tab.png">
	<title>Welcome to BCS Shop</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/webstyle2.css">
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</head>
<body>

	@section('NoiDung')
	<section>
		<div class="container fixDisplay">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div id="carousel-id" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carousel-id" data-slide-to="0" class=""></li>
							<li data-target="#carousel-id" data-slide-to="1" class=""></li>
							<li data-target="#carousel-id" data-slide-to="2" class="active"></li>
						</ol>
						<div class="carousel-inner">
							@for($i = 0; $i < ($slide->count() - 1); $i++)
							<div class="item" >
								<img alt="First slide" src="image/{!!$slide[$i]->image!!}" width="100%">
							</div>
							@endfor
							<div class="item active">
								<img alt="Third slide" src="image/banner3.png" width="100%">
							</div>
						</div>
						<a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
						<a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container list-product">
			<div class="col-xs-12 col-md-3">
				<div>
					<h2><br></h2>
				</div>
				<div class="btn-group-vertical">
					<button type="button" class="btn btn-default">ALL PRODUCTS</button>
					<button type="button" class="btn btn-default">GIÀY THỂ THAO</button>
					<div class="btn-group">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							DROPDOWN
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="#">DROPDOWN LINK</a></li>
							<li><a href="#">DROPDOWN LINK</a></li>
						</ul>
					</div>
					<button type="button" class="btn btn-default">GIÀY ĐI BỘ</button>
				</div>
			</div>
			<div class="col-xs-12 col-md-9">
				<h2>What's new?</h2>
				<table class="table table-bordered">
					<tbody>
						<tr>
							<td>
								<a href="details.html"><img alt="NMD" src="image/adidas3.jpg"></a>
								<p></p>
								<b> Adidas NMD <a href=""><i class="fas fa-shopping-cart"></i></a></b>
								<p> Giá: 2.000.000 VNĐ</p>
							</td>
							<td>
								<img alt="EQT" src="image/adidas2.jpg">
								<p></p>
								<b> Adidas EQT <a href=""><i class="fas fa-shopping-cart"></i></a></b>
								<p> Giá: 2.500.000 VNĐ</p>
							</td>
							<td>
								<img alt="SWIFT RUN BARRIER" src="image/adidas1.jpg">
								<p></p>
								<b> Adidas SWIFT RUN BARRIER <a href=""><i class="fas fa-shopping-cart"></i></a></b>
								<p> Giá: 3.400.000 VNĐ</p>
							</td>			          		  
						</tr>
						<tr>
							<td>
								<a href="details.html"><img alt="NMD" src="image/adidas3.jpg"></a> 
								<p></p>
								<b> Chuck Taylor 1970s Hi <a href=""><i class="fas fa-shopping-cart"></i></a></b>
								<p> Giá: 1.700.000 VNĐ</p>
							</td>
							<td>
								<a href="details.html"><img alt="NMD" src="image/adidas3.jpg"></a>
								<p></p>
								<b> Classic <a href=""><i class="fas fa-shopping-cart"></i></a></b>
								<p> Giá: 1.250.000 VNĐ</p>
							</td>
							<td>
								<a href="details.html"><img alt="NMD" src="image/adidas3.jpg"></a>
								<p></p>
								<b> Worn In <a href=""><i class="fas fa-shopping-cart"></i></a></b>
								<p> Giá: 1.400.000 VNĐ</p>
							</td>
						</tr>			          		  
						<tr>
							<td>
								<a href="details.html"><img alt="NMD" src="image/adidas3.jpg"></a>
								<p></p>
								<b> Nike Air Max <a href=""><i class="fas fa-shopping-cart"></i></a></b>
								<p> Giá: 2.690.000 VNĐ</p>
							</td>
							<td>
								<a href="details.html"><img alt="NMD" src="image/adidas3.jpg"></a>
								<p> </p>
								<p> </p>
								<p> </p>
								<b> HunterX <a href=""><i class="fas fa-shopping-cart"></i></a></b>
								<p> Giá: 1.200.000 VNĐ</p>
							</td>
							<td>
								<a href="details.html"><img alt="NMD" src="image/adidas3.jpg"></a>
								<p></p>
								<b> HunterX 2018 <a href=""><i class="fas fa-shopping-cart"></i></a></b>
								<p> Giá: 1.199.000 VNĐ</p>
							</td>			          		  
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row control">
			<ul class="pagination">
				<li><a href="#">&laquo;</a></li>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#">&raquo;</a></li>
			</ul>
		</div>	
	</section>

	@endsection

</body>
</html>
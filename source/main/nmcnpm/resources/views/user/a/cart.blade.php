@extends('user.layouts.pattern')
<!DOCTYPE html>
<html lang="en">
<head>
	<base href="{{asset('')}}">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="image/icon_tab.png">
	<title>Welcome to BCS Shop</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/webstyle2.css">
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</head>
<body>

	@section('NoiDung')
	<section>
		<div class="container cart">
			<div>
				<h2>Giỏ hàng của bạn</h2>
			</div>
			<div class="row">
				<table class="shop-table">
					<thead>
						<tr>
							<th>
								Ảnh
							</th>
							<th>
								Thông số
							</th>
							<th>
								Đơn giá
							</th>
							<th>
								Số lượng
							</th>
							<th>
								Thành tiền
							</th>
							<th>
								<button type="button" class="delete-row">Xóa</button>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<img src="image/cart/black.jpg" alt="">
							</td>
							<td>
								<div class="shop-details">
									<div class="productname">
										Lincoln Corner Unit Products
									</div>
									
									<div class="color-choser">
										<span class="text">
											Màu sắc : 
										</span>
										<ul>
											<li>
												<a class="black-bg " href="#">
													black
												</a>
											</li>
											
										</ul>
									</div>
									<p>
										Kích cỡ : 
										<strong class="pcode">
											39
										</strong>
									</p>
								</div>
							</td>
							<td>
								<h5>
									<span class="price">200</span>
								</h5>
							</td>
							<td>
								<input class="quantity" type="number" value="1" min="1">
							</td>
							<td>
								<h5>
									<strong class="red">
										<span class="total-price">200</span>
									</strong>
								</h5>
							</td>
							<td>
								<input type="checkbox" name="record">
							</td>
						</tr>
						<tr>
							<td>
								<img src="image/cart/red_Hapu.jpg" alt="">
							</td>
							<td>
								<div class="shop-details">
									<div class="productname">
										Lincoln Corner Unit Products
									</div>
									
									<div class="color-choser">
										<span class="text">
											Màu sắc : 
										</span>
										<ul>
											<li>
												<a class="red-bg" href="#">
													red
												</a>
											</li>
							
										</ul>
									</div>
									<p>
										Kích cỡ : 
										<strong class="pcode">
											39
										</strong>
									</p>
								</div>
							</td>
							<td>
								<h5>
									<span class="price">200</span>
								</h5>
							</td>
							<td>
								<input class="quantity" type="number" value="1" min="1">
							</td>
							<td>
								<h5>
									<strong class="red">
										<span class="total-price">200</span>
									</strong>
								</h5>
							</td>
							<td>
								<input type="checkbox" name="record">
							</td>
						</tr>
						<tr>
							<td>
								<img src="image/cart/pink.jpg" alt="">
							</td>
							<td>
								<div class="shop-details">
									<div class="productname">
										Lincoln Corner Unit Products
									</div>
									
									<div class="color-choser">
										<span class="text">
											Màu sắc : 
										</span>
										<ul>
											
											<li>
												<a class="pink-bg" href="#">
													pink
												</a>
											</li>
										</ul>
									</div>
									<p>
										Kích cỡ : 
										<strong class="pcode">
											39
										</strong>
									</p>
								</div>
							</td>
							<td>
								<h5>
									<span class="price">200</span>
								</h5>
							</td>
							<td>
								<input class="quantity" type="number" value="1" min="1">
							</td>
							<td>
								<h5>
									<strong class="red">
										<span class="total-price">200</span>
									</strong>
								</h5>
							</td>
							<td>
								<input type="checkbox" name="record">
							</td>
						</tr>
						<tr>
							<h5>Tổng số tiền: <span class="total">600</span></h5>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="6">
								<button class="pull-left">
									<a href="">Tiếp tục mua sắm</a>
								</button>
								
								<button class=" pull-right">
									<a href="">Thanh toán luôn</a>  
								</button>
							</td>
						</tr>
					</tfoot>
				</table>
			</div>

		</div>
	</section>

	@endsection

	<script language="javascript">
		$(document).ready(function(){
        
        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
                if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });

        $('.quantity').on('input', function(){
        	var $quantity = $(this).val();
        	var $parent = $(this).parents('tr');
        	var $price = $parent.find('.price').text();
        	var $total_price = $parent.find('.total-price');
        	
        	$total_price.text($quantity *$price);
        
        });
    });    
	</script>

</body>
</html>
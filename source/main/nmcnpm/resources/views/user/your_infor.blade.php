@extends('user.layouts.pattern')
<!DOCTYPE html>
<html>
<head>
  <base href="{{asset('')}}">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="image/icon_tab.png">
  <title>Welcome to BCS Shop</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/webstyle2.css">
  <link rel="stylesheet" href="css/form.css">
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    .hidden {
      display: none;
    }
  </style>
</head>
<body>
  @section('NoiDung')
  <section class="login">
    <h2 id="Chao">Thông tin của bạn</h2>
    <div class="container login">
      <form action="{{route('postYourInfor')}}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input class="hidden" type="text" name="id" value="{{$user->id}}">
        <!-- check lỗi -->
        @if(count($errors) > 0)
          <div class="alert alert-danger">
            @foreach($errors->all() as $err)
              {{$err}}
            @endforeach
          </div>
        @endif
        @if(Session::has('change_infor_success'))
          <div class="alert alert-success">{{Session::get('change_infor_success')}}</div>
        @endif 

        <div class="row">
          <div class="col-25">
            <label for="User-name">Tên đăng nhập*</label>
          </div>
          <div class="col-75">
            <input type="text" id="username" name="username" value="{{$user->username}}" placeholder="Tên đăng nhập" disabled="">
          </div>
        </div>
        
        <div class="row">
          <div class="col-25">
            <label for="email">Email*</label>
          </div>
          <div class="col-75">
            <input type="email" id="email" name="email" value="{{$user->email}}" placeholder="Email" disabled="">
          </div>
        </div>
        <div class="row">
          <div class="col-25" >
            <label for="Họ">Họ*</label>
          </div>
          <div class="col-75">
            <input type="text" id="ho" name="ho" value="{{$user->ho}}" placeholder="Họ" disabled="">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="Tên">Tên*</label>
          </div>
          <div class="col-75">
            <input type="text" id="ten" name="ten" value="{{$user->ten}}" placeholder="Tên" disabled="">
          </div>
        </div>
        
        <div class="row">
          <div class="col-25">
            <label for="địa chỉ">Địa chỉ*</label>
          </div>
          <div class="col-75">
            <input type="text" id="diachi" name="diachi" value="{{$user->diachi}}" placeholder="Địa chỉ" disabled="">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="Số điện thoại">Số điện thoại*</label>
          </div>
          <div class="col-75">
            <input type="text" id="sdt" name="sdt" value="{{$user->sdt}}" placeholder="Số điện thoại" disabled="">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="ngày sinh">Ngày sinh*</label>
          </div>
          <div class="col-75">
            <input type="date" id="ngaysinh" name="ngaysinh" placeholder="{{$user->ngaysinh}}" disabled="">
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            <label for="giới tính">Giới tính*</label>
          </div>
          <div class="col-75">
            <select name="gioitinh" disabled="" id="gioitinh">
              <option value="nam">Nam</option>
              <option value="nữ">Nữ</option>
            </select>
          </div>
        </div>
        <div class="row">
          <input type="submit" value="Xong" onclick="return validate()">
          <input type="button" id="modify" value="Sửa đổi">
        </div>
      </form>
    </div>
  </section>
  @endsection

  <script language="javascript">
    $(document).ready(function(){
      $('#modify').click(function(){
        // $('input').removeAttr('disabled');
        $('input').attr('required','true');
        $('#modify').css('display','none');
        $('#email').removeAttr('disabled');
        $('#ho').removeAttr('disabled');
        $('#ten').removeAttr('disabled');
        $('#diachi').removeAttr('disabled');
        $('#ngaysinh').removeAttr('disabled');
        $('#sdt').removeAttr('disabled');
        $('#gioitinh').removeAttr('disabled');
        });
        });

    function validate() 
    {
      // body...
      var password = document.getElementById("password").value;
      var re_password = document.getElementById("re-password").value;
      if (password!= re_password){
        alert("Mật khẩu không khớp");
        return false;
      }
      return true;
    }

    // document.getElementById("modify").onclick = function () {
    //             document.getElementById("password").style.display = 'block';
    //             document.getElementById("re-password").style.display = 'block';
    //         };
 
            // document.getElementById("btn2").onclick = function () {
            //     document.getElementById("content").style.display = 'block';
            // };

  </script>
</body>
</html>

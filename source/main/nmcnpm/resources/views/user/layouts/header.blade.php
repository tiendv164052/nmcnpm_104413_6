<header class="container-fluid">
    <div class="header-top">
      <!-- <p>Chào mừng đến BCS Shop</p> -->
      <div class="left">
        <a href="{{url('cart')}}"><i class="fas fa-shopping-cart"></i></a>
      </div>
      <div class="left">
        <input type="button" value="Đăng nhập" onclick="location.href = '{{url('login')}}'">
      </div>
      <div class="left">
        <input type="button" value="Đăng kí" onclick="location.href = '{{url('register')}}'">
      </div>
    </div>

    

    <div class="row header-bot">
      <div class="col-md-3">
        <a href="{{url('/')}}"><i class="fas fa-shoe-prints"></i></a>
      </div>
      <div class="col-xs-12 col-md-5">
        <input type="text" placeholder="Tìm kiếm trong BCS Shop">
      </div>
      <div class="col-xs-12 col-md-4">
        <button type="submit" onclick="location.href = '{{url('index')}}' "><i class="fas fa-search"></i></button>
      </div>
    </div>
  </header>
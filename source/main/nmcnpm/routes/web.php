<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//user login
Route::get('login', 'UserController@getLogin');
Route::post('login', ['as' => 'postLogin', 'uses' => 'UserController@postLogin']);

Route::get('register', 'UserController@getRegister');
Route::post('register', ['as' => 'postRegister', 'uses' => 'UserController@postRegister']);



//
Route::get('/', 'PageController@getIndex');
Route::get('cart', 'PageController@getCart');
Route::get('details/{id}', 'PageController@getDetails');
Route::get('index', 'PageController@getIndex');
Route::get('danhmucsanpham/{id_danhmucsanpham}', ['as' => 'getDanhMucSanPham', 'uses' => 'PageController@getDanhMucSanPham']);

Route::get('your_infor/{id}', 'PageController@getYourInfor');
Route::post('your_infor', ['as' => 'postYourInfor', 'uses' => 'PageController@postYourInfor']);


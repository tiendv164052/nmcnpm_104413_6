<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "order";
    public $timestamps = false; 

    public function user(){
        return $this->belongsTo('App\user', 'id_user', 'id');
    }

    public function order_detail(){
        return $this->hasMany('App\order_detail', 'id_order', 'id');
    }
}

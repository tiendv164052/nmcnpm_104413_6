<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = "order_detail";
    public $timestamps = false; 

    public function order(){
        return $this->belongsTo('App\order', 'id_order', 'id');
    }

    public function sanpham_cuthe(){
        return $this->belongsTo('App\sanpham_cuthe', 'id_sanpham_cuthe', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DanhMucSanPham extends Model
{
    protected $table = "danh_muc_san_pham";
    public $timestamps = false; 

    public function sanpham(){
        return $this->hasMany('App\sanpham', 'id_danhmucsanpham', 'id');
    }
}

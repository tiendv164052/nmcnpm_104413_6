<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SanphamCuthe extends Model
{
    protected $table = "sanpham_cuthe";
    public $timestamps = false; 

    public function order_detail(){
        return $this->hasMany('App\order_detail', 'id_sanpham_cuthe', 'id');
    }

    public function sanpham(){
        return $this->belongsTo('App\sanpham', 'id_sanpham', 'id');
    }
}

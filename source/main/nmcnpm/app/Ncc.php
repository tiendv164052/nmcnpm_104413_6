<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ncc extends Model
{
    protected $table = "ncc";
    public $timestamps = false; 

    public function sanpham(){
        return $this->hasMany('App\sanpham', 'id_ncc', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sanpham extends Model
{
    protected $table = "sanpham";
    public $timestamps = false; 

    public function sanpham_cuthe(){
        return $this->hasMany('App\sanpham_cuthe', 'id_sanpham', 'id');
    }

    public function danh_muc_san_pham(){
        return $this->belongsTo('App\danh_muc_san_pham', 'id_danhmucsanpham', 'id');
    }

    public function ncc(){
        return $this->belongsTo('App\ncc', 'id_ncc', 'id');
    }
}

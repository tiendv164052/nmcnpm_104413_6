<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    public function getLogin(){
    	return view('user.login');
    }
    public function postLogin(Request $request){
    	$this->validate($request, [
    		'username' => 'required',
    		'password' => 'required'	
    	], [
    		'username.required' => "Bạn chưa nhập email",
    		'password.required' => "Bạn chưa nhập password"
    	]);
            // echo $request->username;
            // echo $request->password;

        $credentials = array('username' => $request->username, 'password' => $request->password);
    	if(Auth::attempt($credentials)){
    		return redirect('details');
    	}else{
    		return redirect('login')->with('login_faile', "Đăng nhập thất bại");
    	}
    }

    public function getRegister(){
        return view('user.register');
    }

    public function postRegister(Request $request){
        $this->validate($request, [
            'email' => 'email|unique:users,email',
            'username' =>'unique:users,username'

        ], [
            'email.email' => 'Không đúng định dạng email',
            'email.unique' => 'Email đã có người sử dụng',
            'username.unique' => 'Username đã có người sử dụng'
        ]);

        $user = new User;
        $user->username = $request->username;
        $user->password = encrypt($request->password);// mã hóa pass
        $user->email = $request->email; 
        $user->ho = $request->ho;
        $user->ten = $request->ten;
        $user->sdt = $request->sdt;
        $user->diachi = $request->diachi;
        $user->ngaysinh = $request->ngaysinh;
        $user->gioitinh = $request->gioitinh;
        $user->save();

        return redirect()->back()->with('thanhcong', 'Tạo tài khoản thành công');
    }
}

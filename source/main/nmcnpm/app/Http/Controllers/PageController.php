<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use App\Sanpham;
use App\DanhMucSanPham;
use App\User;
use App\Ncc;

class PageController extends Controller
{
    public function getIndex(){
        $slide = Slide::all();
        $danhmucsanpham = DanhMucSanPham::all();
        $sanpham = Sanpham::paginate(9);  
    	return view('user.index', compact('slide', 'sanpham','danhmucsanpham'));

    }

    public function getDanhMucSanPham($id_danhmucsanpham){
        $slide = Slide::all();
        $danhmucsanpham = DanhMucSanPham::all();
        $sanpham = Sanpham::where('id_danhmucsanpham', $id_danhmucsanpham)->paginate(9);
        return view('user.danhmucsanpham', compact('slide', 'sanpham','danhmucsanpham'));

    }

    public function getCart(){
    	return view('user.cart');
    }

    public function getDetails($id){
        $sanpham = Sanpham::find($id)->first();
        $ncc = Ncc::find(Sanpham::find($id)->id_ncc)->first();
    	return view('user.details', compact('sanpham', 'ncc'));
    }

    public function getYourInfor($id){
        $user = User::where('id', $id)->first();
    	return view('user.your_infor', compact('user'));
    }

    public function postYourInfor(Request $request){
        $user = User::find($request->id);
        $user->ho = $request->ho;
        $user->ten = $request->ten;
        $user->sdt = $request->sdt;
        $user->diachi = $request->diachi;
        $user->ngaysinh = $request->ngaysinh;
        $user->gioitinh = $request->gioitinh;
        $user->email = $request->email;
        $user->save();

        return redirect()->back()->with('change_infor_success', 'Đổi thông tin thành công');
    }
}
